import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import util.Util;
import au.com.bytecode.opencsv.CSVReader;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Header;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfWriter;


public class GeneratePDF {
	//rows
	final int HEADER = 6;
	final int CONTENT = 7;
	
	final int COL_LENGTH = 22;
	
	static String CSV_PATH = "/var/www/html/csvs/";
	static String PDF_PATH = "/var/www/html/pdfs/";
	static String STENCIL_PATH = "/var/www/html/img/stencils/";
	
	static String officeName;
	static String officeAddress;
	static String officePhone;
	static String officeFax;

	final static double ROOT2 = Math.sqrt(2);

	PdfWriter pdfWriter;
	
	Map<String, Integer> headers = new HashMap<String, Integer>();
	public static void main(String[] args) 	
	{	
		try
		{
			long startTime = System.currentTimeMillis();
			//modified for JD's environment
			//args is only used in production, the else case is Development			
			
			String file;
			String fileName;
			String initials;
			
			if (args.length>0)
			{
				file = args[0];
				fileName = args[1];
				initials = args[2];
			}
			else
			{
				file = "PLCsv.csv";
				fileName = "PLCsv.pdf";
				initials = "JD";
				CSV_PATH = "/var/www/html/csvs/";
				PDF_PATH = "/var/www/html/pdfs/";
				STENCIL_PATH = "/var/www/html/img/stencils/";
				
			}
			
			new GeneratePDF().createPdf(file, fileName, initials);
			
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println("Excecution Time: " +totalTime);
			
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			System.out.println("PDF may not compile correctly");
			//prints to error log not to screen
			e.printStackTrace();
		}			
	}
		
	public void createPdf(String f, String filename, String inits)
			throws DocumentException, IOException{
			
		ArrayList<String[]> fullArr = new ArrayList<String[]>();
			//change csv to arr
			fullArr = csv(CSV_PATH+f);
			//create list of controllers
			ArrayList<Controller> controllerArr = createControllerArr(fullArr);
			Equipment equip = createEquipList(controllerArr);
			
			//System.out.println(fullArr);
			//start pdf
			Document document = new Document(PageSize.A4, 0, 0, 0, 0);
			document.setPageSize(document.getPageSize().rotate());	
			
			pdfWriter = PdfWriter.getInstance(document,  new FileOutputStream(PDF_PATH+filename));	
			document.open();
			for(int i = 0; i < controllerArr.size(); i++)
			{
				if(i > 0)
				{
					document.newPage();
				}
				//draw controllers
				drawController(controllerArr.get(i), document, pdfWriter, i+1+"", inits);
			}
			
			//document.newPage();
			//drawEquipList(equip, document, writer, inits);
			
			//close pdf
			document.close();						
		}
				
		public void drawController(Controller controller, Document doc, PdfWriter writer, String pageNum, String inits)
		{	
			//System.out.println("draw controller");
			//controller.printThis();
			
			//connect to db
			Connection con = Database.connectToDatabase();		
						
			//find controller details
			String pname = controller.getProjectName();
			String cname = controller.getControllerName();
			String deviceNum = controller.getDeviceNumber();
			String level = controller.getLevelNum();
			String mac = controller.getMacAdd();
			String drawNum = controller.getDrawNum();
			String network = controller.getNetworkNumber();	
			String mssb = controller.getMssb();
			String additional = controller.getAdditional();
			ArrayList<Revision> rs = controller.getRevisions();
			ArrayList<Point> points = controller.getPoints();
			
			try
			{				
				//find stencil
				Statement st = con.createStatement();
				ResultSet sr = st.executeQuery("SELECT * FROM stencils "+
												"LEFT JOIN comment_box ON stencils.stencil_id = comment_box.stencil_id "+
												"WHERE stencil_name = "+Util.escapeAndQuote(cname));
				String stencil_src = "";
				String stencil_id = "";
				double box_x = 0;
				double box_y = 0;
				double box_width = 0;
				double box_height = 0;
				
				while(sr.next())
				{
					stencil_src = sr.getString("stencil_src");
					stencil_id = sr.getString("stencil_id");	
					box_x = sr.getDouble("box_x");
					box_y = sr.getDouble("box_y");
					box_height = sr.getDouble("box_height");
					box_width = sr.getDouble("box_width");
				}
				
				try
				{
					Image stencil = Image.getInstance(STENCIL_PATH+stencil_src);
					float w = doc.getPageSize().getWidth()/stencil.getWidth();
					float h = doc.getPageSize().getHeight()/stencil.getHeight();
					
					drawImage(doc, 0, 0, doc.getPageSize().getWidth(), doc.getPageSize().getHeight(), stencil_src, true);
					
					//add the office details by JD
					if (officeName.equals("QLD"))
					{
						//use both logos at this office
						//Alerton
						//scale the img relative to how the controller stencil was scaled
						drawImage(doc, 3*doc.getPageSize().getWidth()/100, 4*doc.getPageSize().getHeight()/100, w*350, h*100, "alerton.png");
						
						//LEA
						//scale the img relative to how the controller stencil was scaled
						drawImage(doc, 16*doc.getPageSize().getWidth()/100, 5*doc.getPageSize().getHeight()/100,w*100, h*100, "lea.jpg");
						
					}
					else
					{
						//scale the img relative to how the controller stencil was scaled
						drawImage(doc, 3*doc.getPageSize().getWidth()/100, 4*doc.getPageSize().getHeight()/100, w*350, h*100, "alerton.png");
					}
					
					
					
					placeLabel("Ph: "+officePhone,  2*doc.getPageSize().getWidth()/100, 97*doc.getPageSize().getHeight()/100, 12*doc.getPageSize().getWidth()/100, 97*doc.getPageSize().getHeight()/100, writer, 12, Element.ALIGN_CENTER);
					placeLabel("Fax: "+officeFax,  12*doc.getPageSize().getWidth()/100, 97*doc.getPageSize().getHeight()/100, 22*doc.getPageSize().getWidth()/100, 97*doc.getPageSize().getHeight()/100, writer, 12, Element.ALIGN_CENTER);
					
					
					//add comment boxes if there's additional info
					if(!additional.matches(""))					
					{
						drawBox(cmToPx(box_x), flipY(cmToPx(box_y)), cmToPx(box_width), cmToPx(box_height), false, writer);
						addTextColumn("ADDITIONAL", cmToPx(box_x+0.05), flipY(cmToPx(box_y+0.01)), cmToPx(box_width-0.05), cmToPx(box_height), 10f, Element.ALIGN_MIDDLE, Font.BOLD, writer);
						addTextColumn(additional, cmToPx(box_x+0.05), flipY(cmToPx(box_y+0.01+0.6)), cmToPx(box_width-0.05), cmToPx(box_height-0.01), 8f, Element.ALIGN_LEFT, Font.NORMAL, writer);
					}
				
				}
				catch(Exception e)
				{
					System.out.println("Error: Device "+deviceNum+": "+cname+" does not exist in database.");
					System.out.println(e);
				} 
				
				//set labels
				ResultSet labels = st.executeQuery("SELECT * FROM stencil_label_details");
				//MAKE ALL LABEL NAMES UPPER CASE FOR EASIER VALIDATION
				while(labels.next())
				{					
					String l_name = labels.getString("label_name");
					float s = 12;
					double l_x = labels.getDouble("label_x");
					double l_y = labels.getDouble("label_y");
					double l_height = labels.getDouble("label_height");
					double l_width = labels.getDouble("label_width");
					String txt = "";
					int align = Element.ALIGN_CENTER;
					if(l_name.matches("Project Name"))
					{
						txt = pname;
						//placeLabel(level, cmToPx(l_x+3), cmToPx(l_y+0.9), cmToPx(19.2), cmToPx(6.25), writer);
					}
					if(l_name.matches("Level"))
					{
						txt = level;
						txt += " "+mssb;
					}
					if(l_name.matches("MSTP"))
					{
						txt = network;
						align = Element.ALIGN_LEFT;
					}
					if(l_name.matches("Drawing No"))
					{
						txt = drawNum;
						if(drawNum.length() > 11)
						{
							//if drawingNum has too many chars make smaller
							s = 10;
							if(drawNum.length() > 12)
							{
								s = 8;
								l_y = l_y + 0.125;
							}
						}
					}
					if(l_name.matches("MAC Address"))
					{
						txt = mac;
						align = Element.ALIGN_LEFT;
					}
					if(l_name.matches("Device Instance"))
					{
						txt = deviceNum;
						align = Element.ALIGN_LEFT;
					}
					if(l_name.matches("Sheet No"))
					{
						txt = pageNum;
					}
					if(l_name.matches("Drawn By"))
					{
						txt = inits;
					}
					if(l_name.matches("Checked By"))
					{
						//Get the last checked by value of the last revision
						if(rs.size() > 0)
						{
							txt = rs.get(rs.size()-1).getCheckedBy();
						}
					}
					//if txt is defined add to doc
					if(txt != "" && txt != null)
					{							
						placeLabel(txt, cmToPx(l_x), cmToPx(l_y), cmToPx(l_width), cmToPx(l_height), writer, s, align);
					}										
				}				
				
				//POINTS//
				//boolean groundLDrawn = false;
				//boolean groundRDrawn = false;
				boolean activeLDrawn = false;
				boolean activeRDrawn = false;
				for (Point p : points) 
				{
					//for each point in this controller; 
					String pos = p.getPoint_position().replaceAll("AI\\s?|BI\\s?", "IN ");
					//rewrites the point position in controller 
					//p.setPoint_position(pos);
					
					//get the rest of the point info from the controller
					String desc = p.getPoint_description();
					String device = p.getPoint_device();
					String equip = p.getPoint_equipment();
					boolean highlight = p.getHighlight();
					
					//match this to result set point				
					//set device points
					//System.out.println("pos: |"+pos+"|");
					String sql = "SELECT *, stencil_points.point_id AS pid " +
								"FROM stencil_points " +
								"WHERE stencil_points.stencil_id = "+Util.escapeAndQuote(stencil_id)+" "+
								"AND point_name = "+Util.escapeAndQuote(pos);
					ResultSet point = st.executeQuery(sql);
					
					int point_id = -1;
					//int img_id = -1;
					double img_x = 0;
					double img_y = 0;
					String img_dir = "";
					String src = "";
					String ground_line = "-1";
					String active_line = "-1";
					boolean flip_half_pin = true;
					boolean on_ground = false;
					boolean on_active = false;
					int ground_end = 0;
					int active_end = 1;
					double y_offset = 0;
					double x_offset = 0;
					double label_x = 0;
					double label_y = 0;
					double img_height = 0.5;
					double img_width = 1.85;
					boolean half_pin = false;
					String img_flip_src = "";
					String on_point = "";
					int num_points = 0;
					String symbol_img = null;
					Image symb = null;
					boolean use_alt = false;
					boolean has_shield = false;
					int next_active_line = -1;
						
					//point information
					if(point.next())
					{
						point_id = point.getInt("pid");
						img_x = point.getDouble("point_x");
						img_y = point.getDouble("point_y");
						img_dir = point.getString("point_side");	
						ground_line = point.getString("on_ground");
						active_line = point.getString("on_active");
						flip_half_pin = point.getBoolean("flip_half_pin");						
					}
					else
					{
						String errTxt = "ERROR: "; 
						if(deviceNum != "")
						{
							errTxt += "Device No: "+deviceNum;
						}
						else
						{
							errTxt += "Page "+pageNum;
						}
								
						System.out.println(errTxt+" - Point "+pos+" does not exist on card "+cname);
					}
					
					//check shape name exists if does exist then do this.
					
					//System.out.println("device: |"+device.toUpperCase()+"|");
					//System.out.println("image_dir: |"+img_dir+"|");
					ResultSet point_pic = st.executeQuery("SELECT * FROM shape_details " +
														"LEFT JOIN point_types ON shape_details.on_point = point_types.type_id "+
														"WHERE UPPER(shape_name) = "+Util.escapeAndQuote(device.toUpperCase())+" "+
														"AND shape_direction = "+Util.escapeAndQuote(img_dir));
					//shape information
					if(point_pic.next())
					{
						//img_id = point_pic.getInt("shape_detail_id");
						src = point_pic.getString("shape_src");
						y_offset = point_pic.getDouble("y_offset");
						x_offset = point_pic.getDouble("x_offset");
						on_ground = point_pic.getBoolean("has_ground");
						on_active = point_pic.getBoolean("has_active");
						ground_end = point_pic.getInt("ground_end");
						active_end = point_pic.getInt("active_end");
						half_pin = point_pic.getBoolean("half_pin");
						img_flip_src = point_pic.getString("flipped_src");
						on_point = point_pic.getString("type_name");
						num_points = point_pic.getInt("num");
						symbol_img = point_pic.getString("symbol_img");
						has_shield = point_pic.getBoolean("has_shield");
						
						if(point_pic.getDouble("shape_height") != 0)
						{
							img_height = point_pic.getDouble("shape_height");
						}
						if(point_pic.getDouble("shape_width") != 0)
						{
							img_width = point_pic.getDouble("shape_width");
						}	
					
						if(on_point.matches("[AB]I"))
						{
							on_point = "IN";
						}
						if(pos.matches(on_point+" [0-9]+") == false)
						{
							System.out.println("ERROR: Device No: "+deviceNum+" - "+device+" should not be placed on point "+pos);
						}
						
					
						//if a bo && is across two points
						if(pos.matches("BO [0-9]+") && num_points == 2)
						{
							
							//find if any alt points for a bo
							ResultSet bo_point = st.executeQuery("SELECT * FROM on_two_bos " +
																"LEFT JOIN stencil_points ON on_two_bos.move_to = stencil_points.point_id "+
																"WHERE on_two_bos.stencil_id = "+Util.escapeAndQuote(stencil_id)+" "+
																"AND on_two_bos.point_id = "+point_id);
							//System.out.println(src);
							if(bo_point.next())
							{
								//find any alt images and alt x/y positions
								int move_to = bo_point.getInt("move_to");
								use_alt = bo_point.getBoolean("use_alt_img");

								if(move_to != 0 && move_to != point_id)
								{
									if(use_alt)
									{
										//find symbol img 
										try 
										{											
											final double SYM_HEIGHT = 0.37;
											final double SYM_WIDTH = 1.09;
											double next_point_y = bo_point.getDouble("point_y");
											next_active_line = bo_point.getInt("on_active");

											//if com is below bo
											if(!flip_half_pin)
											{
												img_y = img_y - 0.5;												
											}

											if(!bo_point.getBoolean("flip_half_pin"))
											{
												next_point_y = next_point_y - 0.5;
											}
											
											//center point is y1 + (y2-y1)/2 + symbolheight/2
											double center_point = (SYM_HEIGHT/2)+img_y+(next_point_y-img_y)/2;
											
											symb = Image.getInstance(STENCIL_PATH+symbol_img);
											symb.setAbsolutePosition(cmToPx(img_x+0.8), flipY(cmToPx(center_point)));
											symb.scaleAbsolute(cmToPx(SYM_WIDTH), cmToPx(SYM_HEIGHT));
											
											//stop overlap if backwards?
											
											double top_symb = center_point - SYM_HEIGHT;
											double btm_symb = center_point;
																					
											//top of symbol to top connector
											drawLine(cmToPx(img_x+1.4), flipY(cmToPx(img_y)), cmToPx(img_x+1.4), flipY(cmToPx(top_symb)), writer);
											//bottom of symbol to bottom connector
											drawLine(cmToPx(img_x+1.4), flipY(cmToPx(btm_symb)), cmToPx(img_x+1.4), flipY(cmToPx(next_point_y)), writer);
											//connectors to points
											drawLine(cmToPx(img_x), flipY(cmToPx(img_y)), cmToPx(img_x+1.4), flipY(cmToPx(img_y)), writer);
											drawLine(cmToPx(img_x), flipY(cmToPx(next_point_y)), cmToPx(img_x+1.4), flipY(cmToPx(next_point_y)), writer);
																						
											//set ground line and label to center point y
											img_y = center_point;										
										}
										catch (Exception e)
										{
											e.printStackTrace();
										}
										
									}
									else
									{
										//if not an alt image use the current points position
										img_x = bo_point.getDouble("point_x");
										img_y = bo_point.getDouble("point_y");
									}
									
									//find point name to check if it exists
									String bo_point_name = bo_point.getString("point_name");
									
									//add to point list
									if(controller.pointExists(bo_point_name))
									{
										System.out.println("Error: Device "+deviceNum+": More than one device on point "+bo_point_name);
									}
								}
							}								
						}	
					}
					else
					{
						String pointErrTxt = "ERROR: "; 
						if(deviceNum != "")
						{
							pointErrTxt += "Device No: "+deviceNum;
						}
						else
						{
							pointErrTxt += "Page "+pageNum;
						}
						System.out.println(pointErrTxt+" - "+device+" can not be placed on point "+img_dir+" "+pos+" of card "+cname+" - Check Spelling");
					}
					
					
					
					//if there's a directional offset add it 
					if(y_offset != 0)
					{
						img_y = img_y + y_offset;
					}
					if(x_offset != 0)
					{
						img_x = img_x + x_offset;
					}
					
					if(img_dir.matches("L"))
					{
						label_x = img_x-4;
						label_y = img_y-0.1;
					}
					else
					{
						label_x = img_x+2.25;
						label_y = img_y-0.1;
					}
					
					try
					{	
						//Image img;						
						//if alt image is used above rewrite the img
						if(use_alt)
						{
							//img = symb;
							doc.add(symb);
						}
						else
						{
							//draw image
							String imgSrc = src;
							
							
							if(half_pin == true && flip_half_pin == true)
							{
								//flips img
								//img.scaleAbsoluteHeight(-cmToPx(img_height));
								imgSrc = img_flip_src;
								//switch the active/ground line position
								switch(ground_end)
								{
									case 0:
										ground_end = 2;
										break;
									case 2: 
										ground_end = 0;
										break;
									default:
										ground_end = ground_end+0;
								}
								
								switch(active_end)
								{
									case 0:
										active_end = 2;
										break;
									case 2: 
										active_end = 0;
										break;
									default:
										active_end = active_end+0;
								}
							}

							drawImage(doc, cmToPx(img_x), flipY(cmToPx(img_y)), cmToPx(img_width), cmToPx(img_height), imgSrc);
							
						}

						
						//add a highlight if needed
						if(highlight == true)
						{
							drawHighlightBox(cmToPx(img_x-0.01), flipY(cmToPx(img_y-0.01)), cmToPx(img_height+0.01), cmToPx(img_width+0.01), 0.45f, BaseColor.YELLOW, writer);							
						}
									
						//	System.out.println("img "+src);
						//	System.out.println("x "+img_x+" y "+img_y);
						//	System.out.println("w "+img_width+" h "+img_height);
						//	System.out.println(img.getUrl());
						//	doc.add(img);
						//	System.out.println("img detail "+img.getWidth());
						
						String lbl_text = desc;				
						if(!equip.isEmpty())
						{
							lbl_text = equip+" "+desc;
							if(img_dir == "L")
							{
								label_x = label_x-3;
							}
						}
						placeDeviceColumn(lbl_text, cmToPx(label_x), cmToPx(label_y), writer);	
					}
					catch(Exception e)
					{
						//if the image is found db but does't match up to a file
						if(src != "")
						{
							System.out.println("ERROR "+deviceNum+": "+src+" does not exist");
						}
					}
					
					//GOUND LINE//		
					double ground_y = 0;
					double ground_x = 0;
					//find ground line
					ResultSet ground = st.executeQuery("SELECT * FROM ground_lines WHERE line_id = "+Util.escapeAndQuote(ground_line));
					//if there's a ground line and the shape has a ground connection
					//draw ground line
					if(ground.next() && on_ground == true)
					{
						ground_y = ground.getDouble("ground_y");
						ground_x = ground.getDouble("ground_x");
						double endOfGround = img_y-0.25;
						switch(ground_end)
						{
							case 0:
								endOfGround = img_y-(img_height);
								break;
							case 1:
								endOfGround = img_y-((img_height/2)-0.02);
								break;
							case 2:
								endOfGround = img_y;
								break;
						}
						
						//draw line
						drawLine(cmToPx(ground_x), flipY(cmToPx(ground_y)), cmToPx(ground_x), flipY(cmToPx(endOfGround)), writer);
						
						//draw connector to ground
						if(img_dir.matches("L"))
						{
							drawLine(cmToPx(ground_x), flipY(cmToPx(ground_y)), cmToPx(img_x+1.85), flipY(cmToPx(ground_y)), writer);
						}
						
						if(img_dir.matches("R"))
						{		
							
							//don't need connector on right side of controller
							//drawLine(cmToPx(ground_x), flipY(cmToPx(ground_y)), cmToPx(img_x), flipY(cmToPx(ground_y)), writer);
							if(stencil_id.matches("[9]") && pos.matches("BO [0-7]"))
							{
								//if 1688 add the active line to hot terminals
								ResultSet hotActive = st.executeQuery("SELECT * FROM active_lines WHERE line_id IN ("+Util.escapeAndQuote(active_line)+","+next_active_line+")");
								while(hotActive.next())
								{
									double hotActive_x = hotActive.getDouble("active_x");
									double hotActive_y = hotActive.getDouble("active_y");
									//2.68 = distance to start of 24vac
									drawLine(cmToPx(hotActive_x), flipY(cmToPx(hotActive_y)), cmToPx(hotActive_x), flipY(cmToPx(2.68)), writer);
									drawLine(cmToPx(hotActive_x), flipY(cmToPx(hotActive_y)), cmToPx(img_x), flipY(cmToPx(hotActive_y)), writer);
									//draw connector dots to hot points
									drawCircle(cmToPx(hotActive_x), flipY(cmToPx(hotActive_y)), cmToPx(0.05), true, writer);
								}
								
							}
						}												
					}
					
					//ACTIVE LINE//
					double active_y = 0;
					double active_x = img_x+1.05;		
					if(img_dir.matches("R"))
					{
						active_x = img_x+0.81;
					}
					ResultSet active = st.executeQuery("SELECT *  FROM active_lines WHERE line_id = "+Util.escapeAndQuote(active_line));
					if(active.next() && on_active == true)
					{						
						active_x = active.getDouble("active_x");
						active_y = active.getDouble("active_y");
						double endOfActive = img_y-0.25;
						switch(active_end)
						{
							case 0:
								endOfActive = img_y-(img_height - 0.02);
								break;
							case 1:
								endOfActive = img_y-(img_height/2);
								break;
							case 2:
								endOfActive = img_y;
								break;
						}
						drawLine(cmToPx(active_x), flipY(cmToPx(active_y)), cmToPx(active_x), flipY(cmToPx(endOfActive)), writer);
						
											
						if(img_dir.matches("L") && activeLDrawn == false)
						{
							double endOfLine = img_x+0.25;
							drawLine(cmToPx(active_x), flipY(cmToPx(active_y)), cmToPx(endOfLine), flipY(cmToPx(active_y)), writer);
							placeLabel("24VDC", cmToPx(img_x-1.5), cmToPx(active_y-0.25), cmToPx(2), cmToPx(1), writer, 8, Element.ALIGN_LEFT);
							
							//draw arrow head
							drawLine(cmToPx(endOfLine), flipY(cmToPx(active_y-0.1)), cmToPx(endOfLine), flipY(cmToPx(active_y+0.1)), writer);
							drawLine(cmToPx(endOfLine), flipY(cmToPx(active_y+0.1)), cmToPx(endOfLine-0.25), flipY(cmToPx(active_y)), writer);
							drawLine(cmToPx(endOfLine-0.25), flipY(cmToPx(active_y)), cmToPx(endOfLine), flipY(cmToPx(active_y-0.1)), writer);
							
							activeLDrawn = true;
						}
						
						
						if(img_dir.matches("R") && activeRDrawn == false)
						{
							
							if(stencil_id.matches("[793]") == false)
							{
								//If 24VAC on right add fuse
								drawBox(cmToPx(active_x - 0.1), flipY(cmToPx(active_y+0.15)), cmToPx(active_x + 0.1), flipY(cmToPx(active_y +0.45)), writer);
								//add the label to it							
								placeDeviceColumn("2 AMP FUSE", cmToPx(active_x+0.6), cmToPx(active_y+0.61), writer);							
							}
							else
							{
								//stencil_id 7,3,9
								String vLabel = "24VDC";
								if(stencil_id.matches("[9]"))
								{
									vLabel = "24VAC";
								}
								
								//draw 24vdc line and arrow on right hand side
								double endOfLine = img_x+1.5;
								drawLine(cmToPx(active_x), flipY(cmToPx(active_y)), cmToPx(endOfLine), flipY(cmToPx(active_y)), writer);
								placeLabel(vLabel, cmToPx(endOfLine+0.7), cmToPx(active_y-0.25), cmToPx(2), cmToPx(1), writer, 8, Element.ALIGN_LEFT);
								//draw arrowhead
								drawLine(cmToPx(endOfLine), flipY(cmToPx(active_y-0.1)), cmToPx(endOfLine), flipY(cmToPx(active_y+0.1)), writer);
								drawLine(cmToPx(endOfLine), flipY(cmToPx(active_y+0.1)), cmToPx(endOfLine+0.25), flipY(cmToPx(active_y)), writer);
								drawLine(cmToPx(endOfLine+0.25), flipY(cmToPx(active_y)), cmToPx(endOfLine), flipY(cmToPx(active_y-0.1)), writer);
							}
							
							activeRDrawn = true;
						}
							
					}
					
					//ground_shield
					if(has_shield)
					{
						//find shield of stencil side
						ResultSet shield_set = st.executeQuery("SELECT * FROM ground_shields WHERE stencil_id = "+Util.escapeAndQuote(stencil_id)+" "+
																"AND shield_side = "+Util.escapeAndQuote(img_dir));
						if(shield_set.next())
						{
							//double shield_x = shield_set.getDouble("shield_x");
							double shield_y = shield_set.getDouble("shield_y");
							
							
							//end of shield = img_y - 0.17 to get to connector
							double end_shield_y = img_y - 0.17;
							//for below
							double shield_x = img_x + img_width - 0.1;
							double end_shield_x = shield_x - 1.25;						
							
							if(img_dir.matches("L"))
							{
								end_shield_x = shield_x - 1.25;
								//shield_x = img_x + img_width - 0.01(to get to connector)
								shield_x = img_x + img_width - 0.1;
							}
							else
							{	
								end_shield_x = shield_x + 1.25;
								//to get to connector
								shield_x = img_x + 0.1;
							}
							
							
							//draw the vertical shield
							drawLine(cmToPx(shield_x), flipY(cmToPx(shield_y)), cmToPx(shield_x), flipY(cmToPx(end_shield_y)), writer);
							//draw connector to right/left
							
							drawLine(cmToPx(shield_x), flipY(cmToPx(shield_y)), cmToPx(end_shield_x), flipY(cmToPx(shield_y)), writer);
								
							//draw symbol (the lines)
							drawLine(cmToPx(end_shield_x), flipY(cmToPx(shield_y)), cmToPx(end_shield_x), flipY(cmToPx(shield_y + 0.25)), writer);
							drawLine(cmToPx(end_shield_x - 0.3), flipY(cmToPx(shield_y + 0.25)), "H",cmToPx(0.6), writer);
							drawLine(cmToPx(end_shield_x - 0.2), flipY(cmToPx(shield_y + 0.35)), "H", cmToPx(0.4), writer);
							drawLine(cmToPx(end_shield_x - 0.1), flipY(cmToPx(shield_y + 0.45)), "H", cmToPx(0.2), writer);
						}						
					}
				}
				
							
				//REVISIONS//
				//Four entries needed
				int r_count = 4;
				if(rs.size() < 5)
				{
					//if list is smaller then 4 place it in the top spaces in the list
					r_count = rs.size();
				}
				
				try
				{
					int revfont = 6;
					//start from last entry and go backwards
					for(int l = rs.size()-1; l > -1; l--)
					{
						if(r_count > 0)
						{
							Revision r = rs.get(l);
							String rname = r.getName();
							String rdesc = r.getDesc();
							String rdate = r.getDate();		
							int align = Element.ALIGN_CENTER;
							//find in the db
							//HAVE TO USE RESULT SET BEFORE CREATING NEW QUERY
							ResultSet revLabelName = st.executeQuery("SELECT * FROM stencil_label_details WHERE label_name = "+Util.escapeAndQuote("Revision "+r_count));
							if(revLabelName.next())
							{
								double revNameX = revLabelName.getDouble("label_x");
								double revNameY = revLabelName.getDouble("label_y");	
								double revNameHeight = revLabelName.getDouble("label_height");
								double revNameWidth = revLabelName.getDouble("label_width");
								placeLabel(rname, cmToPx(revNameX), cmToPx(revNameY), cmToPx(revNameWidth), cmToPx(revNameHeight), writer, revfont, align);
							}
							ResultSet revLabelDate = st.executeQuery("SELECT * FROM stencil_label_details WHERE label_name = "+Util.escapeAndQuote("Revision Date "+r_count));
						
							if(revLabelDate.next())
							{
								double revDateX = revLabelDate.getDouble("label_x");
								double revDateY = revLabelDate.getDouble("label_y");		
								double revDateHeight = revLabelDate.getDouble("label_height");
								double revDateWidth = revLabelDate.getDouble("label_width");
								placeLabel(rdate, cmToPx(revDateX), cmToPx(revDateY), cmToPx(revDateWidth), cmToPx(revDateHeight), writer, revfont, align);
							}
							ResultSet revLabelDesc = st.executeQuery("SELECT * FROM stencil_label_details WHERE label_name = "+Util.escapeAndQuote("Description "+r_count));
							if(revLabelDesc.next())
							{
								double revDescX = revLabelDesc.getDouble("label_x");
								double revDescY = revLabelDesc.getDouble("label_y");
								double revDescHeight = revLabelDesc.getDouble("label_height");
								double revDescWidth = revLabelDesc.getDouble("label_width");
								placeLabel(rdesc, cmToPx(revDescX), cmToPx(revDescY), cmToPx(revDescWidth), cmToPx(revDescHeight), writer, revfont, align);
							}						
							r_count--;
						}
					}
					
				}
				catch(IndexOutOfBoundsException e)
				{
					//e.printStackTrace();
					System.out.println("ERROR: Page "+pageNum+" - Check Revision History");
				}
				
			
				
				//find where the mac img goes and direction
				ResultSet macOptions = st.executeQuery("SELECT * FROM stencil_boards " +
														"LEFT JOIN boards ON stencil_boards.board_id = boards.board_id "+
														"WHERE stencil_id = "+Util.escapeAndQuote(stencil_id));
				if(macOptions.next())
				{
					int board_id = macOptions.getInt("board_id");
					String board_dir = macOptions.getString("board_dir").toLowerCase();
					int num_switches = macOptions.getInt("num_switches");
					double scale_w = 1;
					double scale_h = 1;
					double real_width = 5;
					double real_height = 11.5;
					if(board_dir.matches("[hH]"))
					{
						real_height = 5;
						real_width = 11.5;
					}							
					//Switch on mac switches
					ArrayList<Integer> macSwitches = Util.findMacSwitch(mac, num_switches);
					double b_x = macOptions.getDouble("board_x");
					double b_y = macOptions.getDouble("board_y");
					double b_width = macOptions.getDouble("board_width");
					double b_height = macOptions.getDouble("board_height");
					
					scale_w = real_width/b_width;
					scale_h = real_height/b_height;
										
					String mac_src = macOptions.getString("board_src");				
					
					try
					{
						//choose correct img
						//put it on page
						drawImage(doc,b_x,b_y,b_width,b_height,mac_src);
						
					}
					catch(Exception e)
					{
						System.out.println("Error: device "+deviceNum+": "+mac_src+" does not exist");
					}
					
					if(macSwitches.size() > 0)
					{						
						ResultSet setSwitches = st.executeQuery("SELECT * FROM switches " +
																"WHERE board_id = "+board_id);
						//for each switch
						while(setSwitches.next())
						{
							//int switch_id = setSwitches.getInt("switch_id");
							int switch_num = setSwitches.getInt("switch_num");
							double s_x = setSwitches.getDouble("switch_x");
							double s_y = setSwitches.getDouble("switch_y");
							
													
							//switches based on 10x10
							//scale it out
													
							double width = 2.9/scale_w;
							double height = 1/scale_h;
							double switch_x = s_x/scale_w;
							double switch_y = s_y/scale_h;
							if(board_dir.matches("[hH]"))
							{
								height = 1/scale_w;
								width = 2.9/scale_h;
								//switch_x = s_x/scale_h;
								//switch_y = s_y/scale_w;
							}
						
							//set the switches
							if(macSwitches.contains(switch_num))
							{							
								//switch is on
								if(board_dir.matches("[Vv]"))
								{
									//start the box half way through the switch
									switch_x += width/2;
									drawBox(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(width/2), cmToPx(height), false, writer);
									drawLine(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(b_x+switch_x+width/2), flipY(cmToPx(b_y-switch_y-height)), writer);
								}
								else
								{
									
									//start the box half way through the switch
									switch_y += width/2;
									//switch_x is at bl so need to add width
									//switch_x += height;
									drawBox(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(height), cmToPx(width/2), false, writer);
									drawLine(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(b_x+switch_x+height), flipY(cmToPx(b_y-switch_y-width/2)), writer);
								}
									
								
							}
							else
							{
								if(board_dir.matches("[vV]"))
								{
									drawBox(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(width/2), cmToPx(height), false, writer);
									drawLine(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(b_x+switch_x+width/2), flipY(cmToPx(b_y-switch_y-height)), writer);
								}
								else
								{
									//switch_x is at bl so need to add width
									//switch_x += height;
									drawBox(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(height), cmToPx(width/2), false, writer);
									drawLine(cmToPx(b_x+switch_x), flipY(cmToPx(b_y-switch_y)), cmToPx(b_x+switch_x+height), flipY(cmToPx(b_y-switch_y-width/2)), writer);
								}
														//switch is off
								//System.out.println("Switch "+switch_num+" is off");
							}
						}
					}
					else
					{
						if(controller.getMacAdd() == "")
						{

							System.out.println("Device "+deviceNum+": No MAC address defined");
						}
						else
						{

							System.out.println("Device "+deviceNum+": MAC address is not an expected value");
						}
					}
				}
				else
				{
					//System.out.println("No mac");
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				//System.out.println("DB ERROR: "+e);
			} 
			finally
			{
				Database.closeDatabaseConnection(con);
			}
		}
		
		public void drawEquipList(Equipment equip, Document doc, PdfWriter writer,String inits)
		{
			//for controllers and devices
			//check if any left
			//find name of [0]
			//count the amount of them
			//print their name and amount
						
			ArrayList<String> cs = new ArrayList<String>();
			ArrayList<String> ds = new ArrayList<String>();
			
			ArrayList<Integer> cNum = new ArrayList<Integer>();
			ArrayList<Integer> dNum = new ArrayList<Integer>();
			
			for(int i = 0; i < equip.getControllers().size(); i++)
			{
				boolean exists = true;
				String co = equip.getControllers().get(i);
				exists = cs.contains(co);
				if(exists)
				{
					Integer n = cNum.get(cs.indexOf(co));
					cNum.set(cs.indexOf(co), n++);
				}
				else
				{
					cs.add(co);
					cNum.add(1);
				}		
			}


			for(int i = 0; i < equip.getDevices().size(); i++)
			{
				String dov = equip.getDevices().get(i);
				int index = ds.indexOf(dov);
				if(index > -1)
				{
					//exists
					int n = dNum.get(index)+1;
					dNum.set(index, n);
				}
				else
				{
					//doesn't exist
					ds.add(dov);
					dNum.add(1);
				}
			}
			
			System.out.println(cs);
			System.out.println(cNum);
			System.out.println(ds);
			System.out.println(dNum);
						
			//Draw Heading 
			addTextColumn("Equipment List", cmToPx(5), flipY(cmToPx(3)), cmToPx(10), cmToPx(1), 26, Element.ALIGN_MIDDLE, Font.BOLD, writer);
			//Draw Controller heading 
			addTextColumn("Controllers", cmToPx(1), flipY(cmToPx(4)), cmToPx(14.8), cmToPx(1), 20, Element.ALIGN_MIDDLE, Font.BOLD, writer);
			//draw device heading 
			addTextColumn("Devices", cmToPx(15.8), flipY(cmToPx(4)), cmToPx(14.8), cmToPx(1), 20, Element.ALIGN_MIDDLE, Font.BOLD, writer);
			//draw initials
			
		}
			/*
		public ArrayList<Controller> createControllerArr(ArrayList<String[]> arr)
		{		
			headers = colNum();
			int c_count = 0;	
			//GET HEADER LABELS
			String[] headerArr = arr.get(HEADER);
			//FOR EACH OF THE LABELS
			for(int i = 0; i < headerArr.length; i++)
			{
				String name = headerArr[i];
				name = name.toLowerCase().trim().replace(" ", "_");

				if(headers.containsKey(name))
				{
					headers.put(name, i);
				}			
			}
			
			//get project name
			String pName = arr.get(1)[1]+" "+arr.get(2)[1].toString();	

			//make list of controllers
			ArrayList<Controller> controllers = new ArrayList<Controller>();
			for(int x = CONTENT; x < arr.size(); x++)
			{
				try
				{
					String[] row = arr.get(x);
					//initialise controller 				
					Controller controller = new Controller();
					if(row[headers.get("field_controller_model")].length() > 0)
					{
						//new controller		
						String cName = row[headers.get("field_controller_model")].toString();			
					
						String nNum = row[headers.get("network_number")].toString();
						String dNum = row[headers.get("device_number")].toString();
						String mac = row[headers.get("mac_address")].toString();
						String level = row[headers.get("level")].toString();					
						String drawNum = dNum;
						String mssb = "";
						//if drawing number is not -1 and it has its own number add it.
						if(headers.get("drawing_number") != -1 && row[headers.get("drawing_number")].toString().length() > 0)
						{
							drawNum = row[headers.get("drawing_number")].toString();
						}
						//for different versions
						//if there is mssb in the header set get this string
						if(headers.get("mssb") != -1)
						{
							mssb = row[headers.get("mssb")].toString();
						}
						else
						{
							//System.out.println("location is set");
							//else get the location string
							mssb = row[headers.get("location")].toString();
						}
											
						controller.setMain(cName, nNum, dNum, mac, drawNum, level, pName, mssb);
						//add to controller arr
						controllers.add(controller);					
						//add to c_count;
						c_count++;									
					}
					else if(row[headers.get("point_device")].toString().length() > 0)
					{
						//check for changed rows
						//if equipment exists
						int equipment = headers.get("equipment");
						if(equipment == -1)
						{
							equipment = headers.get("tags");
						}
						
						
						//find the controller at this count;
						controller = controllers.get(c_count-1);
						
						int[] point_types = {headers.get("ai"), headers.get("bi"), headers.get("ao"), headers.get("bo")};
						
						//find the points
						for(int p : point_types)
						{
							//if string in cell has at least one letter and is not full of whitespace
							//
							if(row[p].toString().length()>0 && row[p].toString().matches("^ +$") == false)
							{
								//get point name
								//add point number							
								String[] pointArr = arr.get(HEADER);
								String pointName = pointArr[p].toString();
								String pointNum = row[p].toString();
								
								String point_pos = pointName.trim()+" "+pointNum.trim();
								
								//find point device
								String point_dev = row[headers.get("point_device")].toString();
								
								//find point desc
								String point_desc = row[headers.get("point_description")].toString();
								
								//find point equipment
								String point_equip = row[equipment].toString();
								
								//find whether highlighted
								String highlight = row[headers.get("highlight")].toString();							
								
								Point point = new Point(point_dev, point_desc, point_pos, point_equip, highlight);
								controller.addPoint(point);
							}							
						}				
					}
					
					if(row[headers.get("date")].toString().length() > 0)
					{
						//find the current controller
						controller = controllers.get(c_count-1);
						//get revision name, desc, date and checked by
						String revName = row[headers.get("revision")].toString();
						
						String revDesc = row[headers.get("description")].toString();
						String revDate = row[headers.get("date")].toString();
						String revCheckBy = row[headers.get("checked_by")].toString();
						
						Revision revision = new Revision(revName, revDesc, revDate, revCheckBy);
						controller.addRevision(revision);					
					}
					
					if(row[headers.get("additional_info")].toString().length()>0)
					{
						//find the current controller
						controller = controllers.get(c_count-1);
						String additional = row[headers.get("additional_info")].toString();						
						controller.appendAdditional(additional);
					}	
					
				}
				catch(IndexOutOfBoundsException e)
				{
					System.out.println("Error: Drawing "+x+": Template columns are incorrect. Please check them and try again.");
				}
			}	
			return controllers;
		}
		*/
		

		public ArrayList<Controller> createControllerArr(ArrayList<String[]> arr)
		{		
			headers = colNum();
			int c_count = 0;	
			//GET HEADER LABELS
			String[] headerArr = arr.get(HEADER);
			
			//FOR EACH OF THE LABELS
			for(int i = 0; i < headerArr.length; i++)
			{
				String name = headerArr[i];
				name = name.toLowerCase().trim().replace(" ", "_");

				if(headers.containsKey(name))
				{
					headers.put(name, i);
				}			
			}
			
			
			//get office details
			String[] oRow = arr.get(4);
			
			
			
			officeName = oRow[2].toString();
			officeAddress = oRow[3].toString();
			officePhone = oRow[9].toString();
			officeFax = oRow[10].toString();
			
			
			
			
			//get project name
			String pName = arr.get(1)[2]+" "+arr.get(2)[2].toString();	

			//make list of controllers
			ArrayList<Controller> controllers = new ArrayList<Controller>();
			for(int x = CONTENT; x < arr.size(); x++)
			{
				try
				{
					String[] row = arr.get(x);
					//initialise controller 				
					Controller controller = new Controller();
					if(row[headers.get("field_controller_model")].length() > 0)
					{
						//new controller		
						String cName = row[headers.get("field_controller_model")].toString();			
					
						String nNum = row[headers.get("point_description")].toString();
						
						String dNum;
						if(headers.get("point_notes") != -1)
						{
							dNum = row[headers.get("point_notes")].toString();
						}
						else
						{
							dNum = "it didnt work :(";
						}
						
						
						
						
						String mac = row[headers.get("hli")].toString();
						String level = "";
						String drawNum = dNum;
						String mssb = "";
						//if drawing number is not -1 and it has its own number add it.
						if(headers.get("point_device") != -1 && row[headers.get("point_device")].toString().length() > 0)
						{
							drawNum = row[headers.get("point_device")].toString();
						}
						//for different versions
						//if there is mssb in the header set get this string
						if(headers.get("mssb") != -1)
						{
							mssb = row[headers.get("mssb")].toString();
						}
						else
						{
							//System.out.println("location is set");
							//else get the location string
							mssb = row[headers.get("point_location")].toString();
						}
											
						controller.setMain(cName, nNum, dNum, mac, drawNum, level, pName, mssb);
						//add to controller arr
						controllers.add(controller);					
						//add to c_count;
						c_count++;									
					}
					else if(row[headers.get("point_device")].toString().length() > 0)
					{
						//check for changed rows
						//if equipment exists
						int equipment = headers.get("equipment");
						if(equipment == -1)
						{
							equipment = headers.get("part_number");
						}

						//find the controller at this count;
						controller = controllers.get(c_count-1);
						
						int[] point_types = {headers.get("ai"), headers.get("bi"), headers.get("ao"), headers.get("bo")};
						
						//find the points
						for(int p : point_types)
						{
							//if string in cell has at least one letter and is not full of whitespace
							//
							if(row[p].toString().length()>0 && row[p].toString().matches("^ +$") == false)
							{
								//get point name
								//add point number							
								String[] pointArr = arr.get(HEADER);
								String pointName = pointArr[p].toString();
								String pointNum = row[p].toString();
								
								String point_pos = pointName.trim()+" "+pointNum.trim();
								
								//find point device
								String point_dev = row[headers.get("point_device")].toString();
								
								//find point desc
								String point_desc = row[headers.get("point_description")].toString();
								
								//find point equipment
								String point_equip = row[headers.get("part_number")].toString();
								
								//find whether highlighted
								String highlight = row[headers.get("highlight")].toString();

								Point point = new Point(point_dev, point_desc, point_pos, point_equip, highlight);
								controller.addPoint(point);
							}							
						}				
					}
					
					if(row[headers.get("date")].toString().length() > 0)
					{
						//find the current controller
						controller = controllers.get(c_count-1);
						//get revision name, desc, date and checked by
						String revName = row[headers.get("revision")].toString();
						
						String revDesc = row[headers.get("description")].toString();
						String revDate = row[headers.get("date")].toString();
						String revCheckBy = row[headers.get("checked_by")].toString();
						
						Revision revision = new Revision(revName, revDesc, revDate, revCheckBy);
						controller.addRevision(revision);					
					}
					
					if(row[headers.get("additional_info")].toString().length()>0)
					{
						//find the current controller
						controller = controllers.get(c_count-1);
						String additional = row[headers.get("additional_info")].toString();						
						controller.appendAdditional(additional);
					}	
					
				}
				catch(IndexOutOfBoundsException e)
				{
					System.out.println("Error: Drawing "+x+": Template columns are incorrect. Please check them and try again.");
					e.printStackTrace();
				}
			}	
			return controllers;
		}
	
		public Equipment createEquipList(ArrayList<Controller> controllers)
		{
			Equipment list = new Equipment();
			for(int i = 0; i < controllers.size(); i++)
			{
				Controller c = controllers.get(i);
				list.addController(c.getControllerName());
				ArrayList<Point> points = controllers.get(i).getPoints();
				for(int x = 0; x < points.size(); x++)
				{
					Point p = points.get(x);		
					list.addDevice(p.getPoint_device());
				}
			}					
			return list;
		}
		
		public float cmToPx(double cm)
		{
			double px = 0;
			px = cm*28.4;		
			return (float)px;
		}

		public ArrayList<String[]> csv(String c){		
			ArrayList<String[]> fullCSV = new ArrayList<String[]>();
			//BufferedReader inputStream = null;
			try{
				CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(new File(c))));
				//inputStream = new BufferedReader(new FileReader(c));
				//String line;
				String[] nextRow;
				while((nextRow = csvReader.readNext()) != null)
				{	
					fullCSV.add(nextRow);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			return fullCSV;
		}
		
		
		private void placeLabel(String text, float l_x, float l_y, float l_width, float l_height, PdfWriter writer, float fsize, int align)
		{
			try 
			{
				PdfContentByte cb = writer.getDirectContent();
				if(fsize == 0)
				{
					fsize = 12;
				}
				Font font = FontFactory.getFont(BaseFont.HELVETICA, fsize, Font.NORMAL);
				Phrase p = new Phrase(text, font);
				ColumnText ct = new ColumnText(cb);
				ct.addText(p);
				ct.setLeading(1f, 1f);
				ct.setAlignment(align);
				ct.setSimpleColumn(l_x, flipY(l_y), l_x+l_width, flipY(l_y+l_height));
				ct.go();
			}
			catch (DocumentException e)
			{
				//e.printStackTrace();
			}
		}
		
		/*private void placeDeviceLabel(String txt, float l_x, float l_y, PdfWriter writer)
		{
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf;
			try
			{
				bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
				cb.saveState();
				cb.beginText();
				cb.moveText(l_x, flipY(l_y));
				cb.setFontAndSize(bf, 8);
				cb.showText(txt);
				cb.endText();
				//cb.moveTo(l_x, flipY(l_y));
				cb.restoreState();
			}
			catch(DocumentException e)
			{
				//System.out.println(e);
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}*/
		
		/**
		 * Wraps text to a 3.9cm column
		 * @param txt text to put in label
		 * @param l_x x position
		 * @param l_y y position
		 * @param writer pdfwriter
		 * @throws DocumentException
		 */
		private void placeDeviceColumn(String txt, float l_x, float l_y, PdfWriter writer)
		{
			int fsize = 7;
			int len = txt.length();
			if(len > 60)
			{
				fsize = fsize-2;
				if(len > 90)
				{
					//System.out.println("Error: device description is longer then recommended (max 60 char) "+txt);
				}
			}
			try
			{
				PdfContentByte cb = writer.getDirectContent();		
				Font font = FontFactory.getFont(BaseFont.HELVETICA, fsize, Font.NORMAL);
				Phrase p = new Phrase(txt, font);
				ColumnText ct = new ColumnText(cb);
				ct.addText(p);
				ct.setLeading(1f,1f);
				ct.setAlignment(Element.ALIGN_CENTER);
				ct.setAlignment(Element.ALIGN_TOP);
				ct.setSimpleColumn(l_x, flipY(l_y-cmToPx(0.4)), l_x+cmToPx(3.9), flipY(l_y+cmToPx(1.5)));
				//ct.setYLine(flipY(l_y-cmToPx(1)));
				ct.go();
			}
			catch(Exception e)
			{
				System.out.println("ERROR: device descrption could not be written: "+txt);
			}
			
		}
		
		private void drawLine(float start_x, float start_y, float end_x,float end_y, PdfWriter writer)
		{
			PdfContentByte cb = writer.getDirectContent();
			cb.saveState();
			cb.setLineWidth(1f);
			cb.moveTo(start_x, start_y);
			cb.lineTo(end_x, end_y);
			cb.stroke();
			cb.restoreState();
		}
		
		private void drawLine(float start_x, float start_y, String dir, float len, PdfWriter writer)
		{
			PdfContentByte cb = writer.getDirectContent();
			cb.saveState();
			cb.setLineWidth(1f);
			cb.moveTo(start_x, start_y);
			if(dir.matches("[Vv]"))
			{
				cb.lineTo(start_x, start_y+len);
			}
			if(dir.matches("[Hh]"))
			{
				cb.lineTo(start_x+len, start_y);
			}
			cb.stroke();
			cb.restoreState();
			
		}
		
		private void drawBox(float ulx, float uly, float brx, float bry, PdfWriter writer)
		{
			//draws a box
			PdfContentByte cb = writer.getDirectContent();
			cb.saveState();
			cb.setLineWidth(1f);
			cb.rectangle(ulx, uly, brx-ulx, bry-uly);
			cb.restoreState();
		}
		
		private void drawHighlightBox(float ulx, float uly, float height, float width, float op, BaseColor colour, PdfWriter writer)
		{

			PdfGState gs = new PdfGState();
			gs.setFillOpacity(op);
				
			PdfContentByte cb = writer.getDirectContent();
			
			cb.saveState();
			cb.setGState(gs);
			
			cb.setLineWidth(0);
			cb.setColorFill(colour);			
			cb.rectangle(ulx, uly, width, height);
			cb.fill();
			//cb.stroke();
			cb.restoreState();
			
		}
		
		public void drawBox(float ulx, float uly, float width, float height, boolean filled, PdfWriter writer)
		{
			
			PdfContentByte cb = writer.getDirectContent();
			cb.saveState();
			cb.setLineWidth(1f);
			cb.setColorFill(BaseColor.BLACK);
			cb.rectangle(ulx, uly, width, height);
			if(filled == true)
			{
				cb.fill();			
			}
			cb.stroke();
			cb.restoreState();
		}
		
		private void drawCircle(float x, float y, float r, boolean filled, PdfWriter writer)
		{
			PdfContentByte cb = writer.getDirectContent();
			cb.saveState();
			cb.setColorFill(BaseColor.BLACK);
			cb.circle(x, y, r);
			if(filled)
			{
				cb.fill();			
			}
			cb.restoreState();
		}
		
		private void addTextColumn(String txt, float x, float y, float w, float h, float fontSize, int hAlign, int style, PdfWriter writer)
		{
			PdfContentByte cb = writer.getDirectContent();		
			Font font = FontFactory.getFont(BaseFont.HELVETICA, fontSize, style);
			Phrase p = new Phrase(txt, font);
			ColumnText ct = new ColumnText(cb);
			ct.addText(p);
			ct.setLeading(1f,1f);
			ct.setAlignment(hAlign);
			ct.setAlignment(Element.ALIGN_TOP);
			ct.setSimpleColumn(x, y, x+w, y+h);
			//ct.setYLine(flipY(l_y-cmToPx(1)));
			try {
				ct.go();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public float flipY(float y )
		{
			y = cmToPx(21) - y;
			return y;
		}
		
		private Map<String, Integer> colNum()
		{
			//sets the default for each col number as-1
			Map<String, Integer> col_headers = new HashMap<String, Integer>();
			col_headers.put("field_controller_model", -1);
			col_headers.put("network_number", -1);
			col_headers.put("device_number", -1);
			col_headers.put("mac_address", -1);
			col_headers.put("drawing_number", -1);
			col_headers.put("end_of_line", -1);
			col_headers.put("level", -1);
			col_headers.put("mssb", -1);
			col_headers.put("point_location", -1); //was location - edited by JD
			col_headers.put("equipment", -1);
			col_headers.put("part_number", -1); //was tags - edited by JD
			col_headers.put("point_notes", -1); //added by JD
			col_headers.put("point_description", -1);
			col_headers.put("point_device", -1);
			col_headers.put("equipment_model", -1);
			col_headers.put("highlight", -1);
			col_headers.put("ai", -1);
			col_headers.put("bi", -1);
			col_headers.put("ao", -1);
			col_headers.put("bo", -1);
			col_headers.put("hli", -1);
			col_headers.put("revision", -1);
			col_headers.put("description", -1);
			col_headers.put("date", -1);
			col_headers.put("checked_by", -1);
			col_headers.put("additional_info", -1);
			
			return col_headers;
		}
		
		//Joshs Functions
		private void drawImage(Document doc, double x, double y, double sx, double sy, String imgSrc) throws Exception
		{
			drawImage(doc, x, y, sx, sy, imgSrc,false);
		}
		
		private void drawImage(Document doc, double x, double y, double sx, double sy, String imgSrc, boolean border) throws Exception
		{
			
			Image img = Image.getInstance(STENCIL_PATH+imgSrc);
			
			img.scaleAbsolute((float)sx, (float)sy);
			img.setAbsolutePosition((float)x, (float)y);
		
			if (border)
			{
				img.setBorderWidth(1f);
			}
		
			//elshan
			//System.out.println(imgSrc);
			
			if (imgSrc.equals("VLC-1188.png"))
			{
				
				doc.add(img);
				//drawVLC1188();
			}
			else
			{
				doc.add(img);
			}
		
		}
		private void drawPageTemplate()
		{
			try
			{
				drawRect(1,1,840,593);
				drawLine(1,575,841,575);
				drawLine(281,594,281,575);
				drawLine(562,594,562,575);

				drawText(5, 580, 10,"MSTP:");
				drawText(286, 580, 10,"MAC ADDRESS:");
				drawText(567, 580, 10,"DEVICE INSTANCE:");
				
				drawLine(1,65,841,65);
				drawLine(270,1,270,65);
				drawLine(525,1,525,65);
				drawLine(550,1,550,65);
				drawLine(800,1,800,65);
				
				drawLine(270,22,525,22);
				drawLine(325,1,325,22);
				drawLine(410,1,410,22);
				drawLine(465,1,465,22);
				drawText(272, 15, 5,"Sheet No:");
				drawText(327, 15, 5,"Drawing No");
				drawText(412, 15, 5,"Drawn By:");
				drawText(467, 15, 5,"Checked By:");
				
				for(int i=13; i <65; i+=13)
				{
					drawLine(525,i,841,i);
				}

				drawText(527, 58, 5,"Rev:");
				drawText(552, 58, 5,"Description:");
				drawText(802, 58, 5,"Date:");
				
			}
			catch (Exception e)
			{
				
			}
			
		}
		private void drawVLC1188()
		{
			try
			{
				drawPageTemplate();
				
				
				drawRect(242,100,368,442);
				drawRoundedRect(330,110,190,425,5);

				drawRoundedRect(335,120,180,405,5);
				
				
				drawAlerton(375, 504, 11);
				

				drawText(390, 460, 15,"VLC-1188");
				

				drawJumper(370,250);
				

				drawCircle(406,557,6,false);
				drawLine(391,542,391,557);
				drawLine(421,542,421,557);

				drawArc(406,557,15,0,180);
				
				
				drawCircle(446,85,6,false);
				drawLine(431,100,431,85);
				drawLine(461,100,461,85);

				drawArc(446,85,15,180,360);
				
				
				//Left side terminals
				for (int i=0; i<8; i++)
				{
					drawTerminal(246,526-14.4*i,true);
				}
				
				for (int i=0; i<8; i++)
				{
					drawTerminal(246,400-14.4*i,true);
				}
				
				for (int i=0; i<8; i++)
				{
					drawTerminal(246,202-14.4*i,true);
				}
				
				//Right side terminals
				for (int i=0; i<9; i++)
				{
					drawTerminal(570,526-14.4*i,false);
				}
				
				for (int i=0; i<9; i++)
				{
					drawTerminal(570,366-14.4*i,false);
				}
				
				for (int i=0; i<9; i++)
				{
					drawTerminal(570,218-14.4*i,false);
				}

				drawRect(580,385,20,20);
				drawLine(524,402,580,402);
				drawLine(524,388,580,388);

				drawLineSegments(new double[] {524,394,538,398,552,394,566,398,580,394});
				drawLineSegments(new double[] {524,392,538,396,552,392,566,396,580,392});

				String[] cardDetails = {"POWER",
						"MAX LOAD - 106VA",
						"NO BO'S - 10VA",
						"24VAC LOADS @ .5A MAX",
						"24VDC SOURCES 100mA",
						"TOTAL",
						"FUSE: AGC-6 AMP FAST",
						"CLASS 2 CIRCUITS ONLY",
						"USE COPPER CONDUCTORS",
						"ONLY",
						"FOR INDOOR USE ONLY",
						"SUITABLE FOR PLENUM",
						"MOUNTING",
						"SW1 = AO SETUP",
						"SW2 = VLC MSC ADDRESS",
						"ON = 4-20mA",
						"OFF = 0-10VDC",
						"4-20mA INPUTS REQUIRE",
						"250 OHM, 1% RESISTOR",
						"ACROSS INPUT AND COM"};
				drawTextLines(380,380,5,5,0,cardDetails);
				
				
				//Left details
				String[] terminal1Details = {"1-IN 0/MSET",
						"2-COM",
						"3-IN 1",
						"4-IN 2",
						"5-COM",
						"6-IN 3",
						"7-COM",
						"8-24 VDC"};
				drawTextLines(285,526+5,6,14.4,0,terminal1Details);
				
				String[] terminal2Details = {"9-IN 4",
						"10-COM",
						"11-IN 5",
						"12-IN 6",
						"13-COM",
						"14-IN 7",
						"15-COM",
						"16-24 VDC"};
				drawTextLines(285,400+5,6,14.4,0,terminal2Details);
				
				String[] terminal3Details = {"17-IN 8",
						"18-COM",
						"19-IN 9",
						"20-IN 10",
						"21-COM",
						"22-24 VDC",
						"23-DATA+",
						"24-DATA-"};
				drawTextLines(285,202+5,6,14.4,0,terminal3Details);
				

				drawText(320, 110, 20,"}");
				drawText(335, 115, 5,"BACnet MS/TP");
				
				//Right details
				String[] terminal4Details = {"24 VAC-25",
						"GND-26",
						"BO 0-27",
						"GND-28",
						"BO 1-29",
						"BO 2-30",
						"GND-31",
						"BO 3-32",
						"GND-33"};
				drawTextLines(535,526+5,6,14.4,0,terminal4Details);
				
				String[] terminal5Details = {"BO 4-34",
						"GND-35",
						"BO 5-36",
						"BO 6-37",
						"GND-38",
						"BO 7-39",
						"AO 0-40",
						"COM-41",
						"AO 1-42"};
				drawTextLines(535,366+5,6,14.4,0,terminal5Details);
				
				String[] terminal6Details = {"AO 2-43",
						"COM-44",
						"AO 3-45",
						"AO 4-46",
						"COM-47",
						"AO 5-48",
						"AO 6-49",
						"COM-50",
						"AO 7-51"};
				drawTextLines(535,218+5,6,14.4,0,terminal6Details);
				
				
				//MSTP
				
				drawLine(120,110,246,110);
				drawLine(106,124,246,124);
				
				drawLine(120,110,120,96);
				drawCircle(120,96,1.5,true);
				drawLine(106,124,106,82);
				drawCircle(106,82,1.5,true);
				

				drawText(65, 87, 5,"MSTP");
				drawArrow(80,96,180,4);
				drawLine(80,96,140,96);
				drawArrow(140,96,0,4);
				
				drawArrow(80,82,180,4);
				drawLine(80,82,140,82);
				drawArrow(140,82,0,4);
				
				drawText(165, 72.5, 5,"Ground Shield at Global Controller End Only");
				drawArrow(100,74,180,4);
				drawLine(100,74,160,74);
				drawArrow(160,74,0,4);
				

				drawRoundedRect(130,108,8,18,4);
				drawLine(126,117,126,74);
				drawLine(126,117,130,117);
				drawCircle(126,74,1.5,true);
				
				
				//Fuse
				drawRect(695,555,24,12);
				drawText(700, 559, 5,"Fuse");
				drawRect(640,555,24,12);
				drawText(645, 559, 5,"Fuse");
				

				drawLine(690,561,695,561);
				drawLine(719,561,730,561);
				drawLine(620,561,640,561);
				
			}
			catch (Exception e)
			{
				
			}
			
			
			
		}
		
		private void drawJumper(double x,double y) throws DocumentException, IOException
		{
			drawText(x+36,y+11,4,"INPUT SETUP JUMPERS");
			drawRect(x,y+10,30,6);
			drawText(x+6,y+17,4,"1");
			drawCircle(x+6,y+13,2,false);
			drawText(x+12,y+17,4,"2");
			drawCircle(x+12,y+13,2,false);
			drawText(x+18,y+17,4,"3");
			drawCircle(x+18,y+13,2,false);
			drawText(x+24,y+17,4,"4");
			drawCircle(x+24,y+13,2,false);
			drawText(x+36,y-5,4,"THERMISTOR or DRY CONTACT");
			drawRoundedRect(x+4,y+5,10,4,2);
			drawLine(x+9,y-3,x+35,y-3);
			drawLine(x+9,y-3,x+9,y);
			drawArrow(x+9,y,90,2);
			drawText(x+36,y+5,4,"0-10VDC");
			drawRoundedRect(x+16,y+5,10,4,2);
			drawLine(x+28,y+7,x+35,y+7);
			drawArrow(x+28,y+7,180,2);
			drawText(x+36,y,4,"0-5VDC or 4-20mA");
			drawRoundedRect(x+10,y,10,4,2);
			drawLine(x+22,y+2,x+35,y+2);
			drawArrow(x+22,y+2,180,2);
		}
		
		private void drawTerminal(double x,double y,boolean left)
		{
			drawRect(x,y,36,14);
			drawRect(x+2,y,14,14);
			drawRect(x+20,y,14,14);
			if(left)
			{
				drawCircle(x+10,y+7,5,false);
				drawCircle(x+26,y+7,2,false);
				drawLine(x+10-5/ROOT2,y+7+5/ROOT2,x+10+5/ROOT2,y+7-5/ROOT2);
				
			}
			else
			{
				drawCircle(x+10,y+7,2,false);
				drawCircle(x+26,y+7,5,false);
				drawLine(x+26-5/ROOT2,y+7+5/ROOT2,x+26+5/ROOT2,y+7-5/ROOT2);
			}
		}
		
		//Basic PDF Drawing Library
		/**
		 * PDF Draw Line
		 * 
		 * @param x1 The x coordinate of the first point
		 * @param y1 The y coordinate of the first point
		 * @param x2 The x coordinate of the second point
		 * @param y2 The y coordinate of the second point
		 */
		private void drawLine(double x1, double y1, double x2,double y2)
		{
			drawLine(x1, y1, x2,y2,.1f);
		}
			
	 	/**
		 * PDF Draw Line with line width
		 * 
		 * @param x1 The x coordinate of the first point
		 * @param y1 The y coordinate of the first point
		 * @param x2 The x coordinate of the second point
		 * @param y2 The y coordinate of the second point
		 * @param w The line width
		 */
		private void drawLine(double x1, double y1, double x2,double y2,float w)
		{
			
			PdfContentByte cb = this.pdfWriter.getDirectContent();
			cb.saveState();
			cb.setLineWidth(w);
			cb.moveTo((float) x1,(float) y1);
			cb.lineTo((float) x2,(float) y2);
			cb.stroke();
			cb.restoreState();
			
		}
			
		/**
		 * PDF Draw Circle
		 * 
		 * @param x1 The x coordinate of the center point
		 * @param y1 The y coordinate of the center point
		 * @param r The radius of the circle
		 * @param filled boolean to make the circle solid
		 */
		private void drawCircle(double x, double y, double r, boolean filled)
		{
			
			PdfContentByte cb = this.pdfWriter.getDirectContent();
			//cb.saveState();
			cb.setLineWidth(.1f);
				
			cb.setColorFill(BaseColor.BLACK);
			cb.circle((float) x,(float) y,(float) r);
			if(filled)
			{
				cb.fill();			
			}
			cb.stroke();
				//cb.restoreState();
		}
		
		/**
		 * PDF Draw Text
		 * 
		 * @param x1 The x coordinate of the bottom left point of the text
		 * @param y1 The y coordinate of the bottom left point of the text
		 * @param size The size of the text
		 * @param text The text to be written
		 */
		private void drawText(double x1, double y1, double size, String text) throws DocumentException, IOException
		{
			
			PdfContentByte cb = this.pdfWriter.getDirectContent();
		    BaseFont bf = BaseFont.createFont();
		    cb.beginText();
		    cb.setFontAndSize(bf, (float) size);
		    cb.moveText((float)x1, (float)y1); 
		   cb.showText(text);
			cb.endText();
			
		}
		
		/**
		 * PDF Draw Text
		 * 
		 * @param x1 The x coordinate of the bottom left point of the text
		 * @param y1 The y coordinate of the bottom left point of the text
		 * @param size The size of the text
		 * @param text The text to be written
		 */
		private void drawCenteredText(double x1, double y1, double w, double h, double size, String text) throws DocumentException, IOException
		{
			double y = (y1+y1+h)/2;
			PdfContentByte cb = this.pdfWriter.getDirectContent();
			Font font = FontFactory.getFont(BaseFont.HELVETICA, (float) size, Font.NORMAL);
			Phrase p = new Phrase(text, font);
			ColumnText ct = new ColumnText(cb);
			ct.addText(p);
			ct.setLeading(1f, 1f);
			ct.setAlignment(1);
			ct.setSimpleColumn((float)x1, (float)(y-size), (float)(x1+w), (float)(y+size));
			ct.go();
		}
		
		
		//Secondary Dual Drawing Library
		/**
		 * Draw Dashed Line
		 * 
		 * @param x1 The x coordinate of the first point
		 * @param y1 The y coordinate of the first point
		 * @param x2 The x coordinate of the second point
		 * @param y2 The y coordinate of the second point
		 * @param dashFactor The amount the line is 'dashed'
		 */
		public void drawDashedLine(double x1, double y1, double x2,double y2, double dashFactor)
		{
			if (dashFactor%2==0)
				dashFactor = dashFactor+1;
			
			double xf = (x2-x1)/dashFactor;
			double yf = (y2-y1)/dashFactor;
			double xt = x1;
			double yt = y1;
						
			for (int i =0; i<dashFactor; i=i+1)
			{
				if (i%2==0)
				{
					double x = xt + xf;
					double y = yt + yf;
					drawLine(xt, yt, x, y);
				}
				xt = xt +xf;
				yt = yt +yf;	
			}
		}
		
		/**
		 * Draw Line Segment
		 * 
		 * @param coords Array of coordinates for successive points to be joined
		 */
		public void drawLineSegments(double[] coords)
		{
			//ensure there is atleast 2 points and an even number of coords
			if (coords.length >3 && coords.length%2 !=1 )
			{
				//get first point
				double x1 = coords[0];
				double y1 = coords[1];
				//loop through each pair of points
				for (int i =2; i<coords.length; i += 2)
				{
					double xt = coords[i];
					double yt = coords[i+1];
					drawLine(x1, y1, xt, yt);
					x1=xt;
					y1=yt;
				}
			}
		}
		
		/**
		 * Draw Rectangle
		 * 
		 * @param x1 The x coordinate of the bottom left point
		 * @param y1 The y coordinate of the bottom left point
		 * @param w  The Rectangle Width
		 * @param h  The Rectangle Height
		 */
		public void drawRect(double x1, double y1, double w, double h)
		{
			drawLine(x1,y1,x1,y1+h);
			drawLine(x1,y1+h,x1+w,y1+h);
			drawLine(x1+w,y1+h,x1+w,y1);
			drawLine(x1+w,y1,x1,y1);
		}
		
		/**
		 * Draw Rotated Rectangle
		 * 
		 * @param xc The x coordinate of the center point
		 * @param yc The y coordinate of the center point
		 * @param w The rectangles width
		 * @param h The rectangles height
		 * @param theta The angle of rotation in degrees
		 */
		public void drawRotatedRect(double xc, double yc, double w, double h, double theta)
		{
			theta = 2*Math.PI*theta/360;
			double hyp = Math.sqrt((h/2)*(h/2)+(w/2)*(w/2));
			double phi = Math.asin((h/2)/hyp);
			
			double x1 = xc+hyp*Math.cos(theta+phi);
			double y1 = yc+hyp*Math.sin(theta+phi);
			double x2 = xc+hyp*Math.cos(theta-phi+Math.PI);
			double y2 = yc+hyp*Math.sin(theta-phi+Math.PI);
			double x3 = xc+hyp*Math.cos(theta+phi+Math.PI);
			double y3 = yc+hyp*Math.sin(theta+phi+Math.PI);
			double x4 = xc+hyp*Math.cos(theta-phi);
			double y4 = yc+hyp*Math.sin(theta-phi);
			
			drawLine(x1,y1,x2,y2);
			drawLine(x2,y2,x3,y3);
			drawLine(x3,y3,x4,y4);
			drawLine(x4,y4,x1,y1);
		}
		
		/**
		 * Draw Arc
		 * 
		 * @param x1 The x coordinate of the center point
		 * @param y1 The y coordinate of the center point
		 * @param r  The radius of the Arc
		 * @param theta1 The starting angle in degrees
		 * @param theta2 The finishing angle in degrees
		 */
		public void drawArc(double x1, double y1, double r, double theta1, double theta2)
		{
			double theta1r = 2*Math.PI*theta1/360;
			double theta2r = 2*Math.PI*theta2/360;
			
			double xl = x1+r*Math.cos(theta1r);
			double yl = y1+r*Math.sin(theta1r);
			
			//
			
			for (double i = theta1r; i<=theta2r; i=i+(theta2r - theta1r)/16)
			{
				double x2 = x1+r*Math.cos(i);
				double y2 = y1+r*Math.sin(i);
				drawLine(xl,yl,x2,y2);
				xl = x2;
				yl = y2;
			}
			drawLine(xl,yl,(x1+r*Math.cos(theta2r)),(y1+r*Math.sin(theta2r)));
			
			
			
		}
		
		/**
		 * Draw Loop
		 * 
		 * @param x1 The x coordinate of the first point
		 * @param y1 The y coordinate of the first point
		 * @param x2 The x coordinate of the second point
		 * @param y2 The y coordinate of the second point
		 * @param size The radius at the end points
		 */
		public void drawLoop(double x1, double y1, double x2, double y2, double size)
		{
			drawArc(x1, y1, size, 90,270);
			
			drawLine(x1,y1+size,x2,y2+size);
			drawLine(x1,y1-size,x2,y2-size);
			
			drawArc(x2, y2, size,-90,90);
		}
			
		/**
		 * Draw Arrow
		 * 
		 * @param x1 The x coordinate of the point where the line joins the Arrow
		 * @param y1 The y coordinate of the point where the line joins the Arrow
		 * @param theta angle of rotation of the arrow in degrees
		 * @param size the size of the Arrow
		 */
		public void drawArrow(double x1, double y1, double theta, double size)
		{
			theta = 2*Math.PI*theta/360;
			
			double x2 = x1+size*Math.cos(theta);
			double y2 = y1+size*Math.sin(theta);
			double x3 = x1+(size/2)*Math.cos(theta+Math.PI*2/3);
			double y3 = y1+(size/2)*Math.sin(theta+Math.PI*2/3);
			double x4 = x1+(size/2)*Math.cos(theta-Math.PI*2/3);
			double y4 = y1+(size/2)*Math.sin(theta-Math.PI*2/3);
			
			drawLine(x1,y1,x3,y3);
			drawLine(x3,y3,x2,y2);
			drawLine(x2,y2,x4,y4);
			drawLine(x4,y4,x1,y1);
		}
		
		/**
		 * Draw Rounded Rectangle
		 * 
		 * @param x1 The x coordinate of the bottom left point
		 * @param y1 The y coordinate of the bottom left point
		 * @param w The rectangles width
		 * @param h The rectangles height
		 * @param r The radius of the rounded corners
		 */
		public void drawRoundedRect(double x1, double y1, double w, double h, double r){

			drawLine(x1+r, y1,  x1+w-r,y1);
			drawLine(x1,y1+r,x1,y1+h-r);
			drawLine(x1+w,y1+r,x1+w,y1+h-r);
			drawLine(x1+r,y1+h,x1+w-r,y1+h);
			
			drawArc(x1+r, y1+r, r,180,270);
			drawArc(x1+w-r, y1+r, r,270,360);
			drawArc(x1+r, y1+h-r, r,90,180);
			drawArc(x1+w-r, y1+h-r, r,0,90);		
		}
		
		public void drawAlerton(double x, double y, double size){

			size = size/66.0;
			//A
			drawArc(x+size*60, y+size*49, size*18, 0, 122);
					
			drawLine(x+size*78, y+size*00, x+size*78, y+size*49);
			drawLine(x+size*60, y+size*00, x+size*78, y+size*00);
			drawLine(x+size*60, y+size*14, x+size*60, y+size*00);
			drawLine(x+size*32, y+size*14, x+size*60, y+size*14);
			drawLine(x+size*20, y+size*00, x+size*32, y+size*14);
			drawLine(x+size*00, y+size*00, x+size*20, y+size*00);
			drawLine(x+size*00, y+size*00, x+size*00, y+size*04);
			drawLine(x+size*00, y+size*04, x+size*50.7, y+size*64.4);
					
			//A-hole
			drawLine(x+size*42, y+size*26, x+size*60, y+size*26);
			drawLine(x+size*42, y+size*26, x+size*60, y+size*48);
			drawLine(x+size*60, y+size*26, x+size*60, y+size*48);
					
			//L
			drawLine(x+size*129, y+size*2, x+size*129, y+size*7);
			drawLine(x+size*127, y, x+size*129, y+size*2);
			drawLine(x+size*96, y, x+size*127, y);
			drawArc(x+size*99.4, y+size*17.8, size*18, 186, 260);
			drawLine(x+size*81.5, y+size*15.8, x+size*81.5, y+size*65);
			drawLine(x+size*81.5, y+size*65, x+size*100, y+size*65);
			drawLine(x+size*100, y+size*65, x+size*100, y+size*24.9);
			drawArc(x+size*111.8, y+size*27.2, size*12, 190, 254);
			drawLine(x+size*107.5, y+size*16, x+size*124.5, y+size*16);
			drawLine(x+size*124.5, y+size*16, x+size*127, y+size*17);
			
			//E-Bottom
			drawLine(x+size*151.5, y+size*16, x+size*196, y+size*16);
			drawLine(x+size*196, y, x+size*196, y+size*16.5);
			drawLine(x+size*143, y, x+size*196, y);
			drawArc(x+size*143, y+size*18, size*18, 218, 270);
			//gap from L
			drawLine(x+size*127, y+size*17, x+size*127, y+size*41);
			drawLine(x+size*127, y+size*41, x+size*196, y+size*41);
			drawLine(x+size*196, y+size*41, x+size*196, y+size*25);
			drawLine(x+size*148.5, y+size*25, x+size*196, y+size*25);
			drawArc(x+size*152, y+size*21, size*5, 125, 270);
			
			//E-Top
			drawArc(x+size*143, y+size*50, size*16, 90, 180);
			drawLine(x+size*127, y+size*50, x+size*196, y+size*50);
			drawLine(x+size*196, y+size*50, x+size*196, y+size*66);
			drawLine(x+size*143, y+size*66, x+size*196, y+size*66);
			
			//R
			drawLine(x+size*279.2, y+size*37, x+size*279.2, y+size*49);
			drawArc(x+size*268.5, y+size*40, size*11, 296, 350);
			drawArc(x+size*262, y+size*14, size*20, 12, 55);
			drawLine(x+size*285, y, x+size*281.5, y+size*18.7);
			drawLine(x+size*267, y, x+size*285, y);
			drawLine(x+size*263.5, y+size*18.7, x+size*267, y);
			drawArc(x+size*249, y+size*14.8, size*15, 15, 46);
			drawLine(x+size*219, y+size*25, x+size*260, y+size*25);
			drawLine(x+size*219, y, x+size*219, y+size*25);
			drawLine(x+size*201, y, x+size*219, y);
			drawLine(x+size*201, y+size*41, x+size*201, y);
			drawLine(x+size*201, y+size*41, x+size*255, y+size*41);
			drawArc(x+size*255, y+size*45.5, size*4.5, 270, 450);
			drawLine(x+size*201, y+size*50, x+size*255, y+size*50);
			drawLine(x+size*201, y+size*50, x+size*201, y+size*66);
			drawLine(x+size*201, y+size*66, x+size*270, y+size*66);
			drawArc(x+size*270, y+size*55, size*11, 42, 90);
			
			//T
			drawLine(x+size*358.5, y+size*66, x+size*358.5, y+size*61);
			drawLine(x+size*278, y+size*66, x+size*358.5, y+size*66);
			drawLine(x+size*278, y+size*62, x+size*278, y+size*66);
			//gap from R
			drawLine(x+size*279.2, y+size*49, x+size*308.5, y+size*49);
			drawLine(x+size*308.5, y+size*49, x+size*308.5, y);
			drawLine(x+size*308.5, y, x+size*327, y);
			drawLine(x+size*327, y, x+size*327, y+size*49);
			drawLine(x+size*327, y+size*49, x+size*354, y+size*49);
			
			
			//O
			drawArc(x+size*422, y+size*50, size*16, 0, 90);
			drawLine(x+size*370, y+size*66, x+size*422, y+size*66);
			drawArc(x+size*370, y+size*50, size*16, 90, 135);
			//gap from T
			drawLine(x+size*354, y+size*49, x+size*354, y+size*16);
			drawArc(x+size*370, y+size*16, size*16, 180, 270);
			drawLine(x+size*370, y, x+size*422, y);
			drawArc(x+size*422, y+size*16, size*16, 270, 360);
			drawLine(x+size*438, y+size*16, x+size*438, y+size*50);
			
			//O-Hole
			drawArc(x+size*380, y+size*41, size*8, 90, 180);
			drawLine(x+size*380, y+size*49, x+size*411, y+size*49);
			drawArc(x+size*411, y+size*41, size*8, 0, 90);
			drawLine(x+size*372, y+size*25, x+size*372, y+size*41);
			drawArc(x+size*380, y+size*25, size*8, 180, 270);
			drawLine(x+size*380, y+size*17, x+size*411, y+size*17);
			drawArc(x+size*411, y+size*25, size*8, 270, 360);
			drawLine(x+size*419, y+size*41, x+size*419, y+size*25);
			
			//N
			drawArc(x+size*461, y+size*52, size*14, 45, 90);
			drawLine(x+size*455, y+size*66, x+size*461, y+size*66);
			drawArc(x+size*455, y+size*52, size*14, 90, 180);
			drawLine(x+size*441, y, x+size*441, y+size*52);
			drawLine(x+size*441, y, x+size*459, y);
			drawLine(x+size*459, y, x+size*459, y+size*44);
			drawArc(x+size*461, y+size*44, size*2, 45, 180);
			drawLine(x+size*462, y+size*46, x+size*497.2, y+size*4);
			drawArc(x+size*507, y+size*14, size*14, 225, 270);
			drawLine(x+size*507, y, x+size*512, y);
			drawArc(x+size*512, y+size*14, size*14, 270, 360);
			drawLine(x+size*526, y+size*66, x+size*526, y+size*14);
			drawLine(x+size*507, y+size*66, x+size*526, y+size*66);
			drawLine(x+size*507, y+size*66, x+size*507, y+size*24);
			drawArc(x+size*505, y+size*24, size*2, 225, 360);
			drawLine(x+size*470.8, y+size*62, x+size*504, y+size*22.2);		
		}
		
		public void drawTextLines(double x1, double y1, double fontSize, double lineSize, int lineStart, String[] text) throws DocumentException, IOException
		{
			double x = x1;
			double y = y1;
			if (lineStart >0)
			{
				x = x1+fontSize;
			}
			for (int i = 0; i <text.length; i++)
			{
				if (lineStart == 1)
				{
					drawCircle(x-fontSize, y+fontSize/2, fontSize/6, true);
				}
				else if (lineStart == 2)
				{
					drawText(x-fontSize, y, fontSize, (i+1)+".");
				}
				drawText(x, y, fontSize, text[i]);
				y =y - lineSize;
			}
		}
		
}