
public class Shape {
	String name;
	String src; 
	double y_offset; 
	double x_offset;
	String height;
	String width;
	String shape_dir;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public double getY_offset() {
		return y_offset;
	}

	public void setY_offset(double y_offset) {
		this.y_offset = y_offset;
	}

	public double getX_offset() {
		return x_offset;
	}

	public void setX_offset(double x_offset) {
		this.x_offset = x_offset;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getShape_dir() {
		return shape_dir;
	}

	public void setShape_dir(String shape_dir) {
		this.shape_dir = shape_dir;
	}

	public Shape() {
		// TODO Auto-generated constructor stub
	}

	public Shape(String name, String src, double y_offset, double x_offset,
			String height, String width, String shape_dir) {
		this.name = name;
		this.src = src;
		this.y_offset = y_offset;
		this.x_offset = x_offset;
		this.height = height;
		this.width = width;
		this.shape_dir = shape_dir;
	}

}
