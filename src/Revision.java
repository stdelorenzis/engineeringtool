
public class Revision {

	private String name;	
	private String desc;	
	private String date;
	private String checkedBy;
	
	public String getCheckedBy() {
		return checkedBy;
	}

	public void setCheckedBy(String checkedBy) {
		this.checkedBy = checkedBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Revision(String name, String desc, String date, String checkedBy) {
		this.name = name;
		this.desc = desc;
		this.date = date;
		this.checkedBy = checkedBy;
	}
	
	public void printThis(){
		System.out.println(
				this.name+" "+
				this.desc+" "+
				this.date+" "+
				this.checkedBy);		
	}

}