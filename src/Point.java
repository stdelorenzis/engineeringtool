
public class Point {
	String point_device;
	String point_description;
	String point_position;
	String point_equipment;
	boolean highlight;
	public Point() {
		//do nothing
	}

	public String getPoint_device() {
		return point_device;
	}

	public void setPoint_device(String point_device) {
		this.point_device = point_device;
	}

	public String getPoint_description() {
		return point_description;
	}

	public void setPoint_description(String point_description) {
		this.point_description = point_description;
	}
	
	public void appendDescription(String description)
	{
		//39 in one line
		String curr = this.getPoint_equipment()+" "+this.getPoint_description();			
		String test = curr+" "+description;		
		int m = 39;
		if(test.length() > 60)
		{
			//if length > 60 font size drops to 5. Can fit 42 chars.
			m = 42; 
		}
		for(int i = curr.length(); i > 0; i--)
		{
			//if i is less than last multiple of m add spaces until it reaches it
			if(m % i == 0)
			{
				m = m+i;
				break;
			}			
		}
		
		while(curr.length() < m)
		{
			curr += " ";
		}
		String desc = curr.replace(this.getPoint_equipment(), "");
	    this.setPoint_description(desc += description);
	}
	
	
	public String getPoint_position() {
		return point_position;
	}

	public void setPoint_position(String point_position) {
		this.point_position = point_position;
	}
	
	public String getPoint_equipment() {
		return point_equipment;
	}

	public void setPoint_equipment(String point_equipment) {
		this.point_equipment = point_equipment;
	}
	
	public void setHightlight(String highlight)
	{
		if(highlight.toUpperCase().matches("YES"))
		{
			this.highlight = true;
		}
		else
		{
			this.highlight = false;
		}
		
	}

	public boolean getHighlight()
	{
		return this.highlight;
	}

	public Point(String point_device, String point_description,	String point_position, String point_equipment, String highlight) {
		this.point_device = point_device;
		this.point_description = point_description;
		this.point_position = point_position;
		this.point_equipment = point_equipment;
		if(highlight.toUpperCase().matches("YES"))
		{
			this.highlight = true;
		}
		else
		{
			this.highlight = false;
		}
	}
	
	public void printThis(){
		System.out.println(
				this.point_description+" "+
				this.point_device+" "+
				this.point_position+" "+
				this.point_equipment+" "+
				this.highlight);
		
	}

}
