import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	public static boolean mysqlDriverInstalled()
	{
		try
		{
			Class.forName("com.postgresql.jdbc.Driver");
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("DB ERROR: "+e);
			return false;
		}
		return true;
	}
	
	public static boolean isDatabaseOnline()
	{
		if (mysqlDriverInstalled() == false)
		{
			return false;
		}
		
		try
		{
			DriverManager.getConnection("jdbc:postgresql://192.168.3.4:5432/etool", "apl", "apl");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//System.out.println("DB ERROR: "+e);
			return false;
		}
		
		return true;
		
	}
	
	public static Connection connectToDatabase()
	{
		Connection con = null;
		try
		{
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/etool", "apl", "apl");
			
		}
		catch(SQLException e)
		{
			System.out.println("DB ERROR "+e);
		}
		return con;
		
	}
	
	public static void closeDatabaseConnection(Connection con)
	{
		try
		{
			if(con != null)
			{
				con.close();
			}
		}
		catch(SQLException e)
		{
			System.out.println("DB ERROR: "+e);
		}
		
	}
}
