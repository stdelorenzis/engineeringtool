import java.util.ArrayList;

class Controller {
	
	private String projectName;
	private String controllerName;
	private String networkNumber; 
	private String deviceNumber;
	private String macAdd;
	private String drawNum;
	private String levelNum;
	private String mssb;
	private String system;
	private String additional;
	private ArrayList<Point> points; 
	private ArrayList<Revision> revisions;
	
	
	public Controller(){
		//do nothing
	}
	
	public Controller(String name, String network, String dnum, String madd, String drawN, String lnum, ArrayList<Point> p, ArrayList<Revision> r, String m, String sys){
		controllerName = name;
		networkNumber = network; 
		deviceNumber = dnum;
		macAdd = madd;
		levelNum = lnum;
		points = p;
		revisions = r;
		mssb = m;
		system = sys;
		drawNum = drawN;
		
	}
	
	
	public void setMain(String cName, String nNum, String dNum, String mac, String drawNum, String level, String pName, String mssb) {
		this.projectName = pName;
		this.controllerName = cName;
		this.networkNumber = nNum; 
		this.deviceNumber = dNum;
		this.macAdd = mac;
		this.drawNum = drawNum;
		this.levelNum = level;
		this.mssb = mssb;		
	}
	
	public void setAdditional(String additional){
		this.additional = additional;		
	}
	public void appendAdditional(String add)
	{
			
		String a = this.getAdditional();
		try
		{
			if(a.matches(""))
			{
				this.additional = add;
			}
			else
			{
				this.setAdditional(a+" "+add);
			}			
		}
		catch(NullPointerException e)
		{
			this.additional = add;			
		}	
		
	}
	
	public String getAdditional()
	{
		if(additional != null)
		{

			return additional;
		}
		else
		{
			return "";
		}
	}
	
	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public String getNetworkNumber() {
		return networkNumber;
	}

	public void setNetworkNumber(String networkNumber) {
		this.networkNumber = networkNumber;
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getMacAdd() {
		return macAdd;
	}

	public void setMacAdd(String macAdd) {
		this.macAdd = macAdd;
	}
	
	public String getDrawNum(){
		return drawNum;
	}
	
	public void setDrawNum(String drawNum){
		this.drawNum = drawNum;
	}

	public String getLevelNum() {
		return levelNum;
	}

	public void setLevelNum(String levelNum) {
		this.levelNum = levelNum;
	}
	
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public String getMssb(){
		return mssb;		
	}
	
	public void setMssb(String mssb){
		this.mssb = mssb;	
	}
	
	
	public String getSystem(){
		return system;
	}
	
	public void setSystem(String system){
		this.system = system;
	}


	public ArrayList<Point> getPoints() {
		//if points exist return them else return an empty array of points
		try{
			points.size();
			return points;
		}
		catch(NullPointerException e){
			
			return new ArrayList<Point>();
		}
	}
	
	public Point getPoint(int i)
	{
		Point p = this.getPoints().get(i);
		return p;
	}
	


	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}
	
	public void addPoint(Point point){
		ArrayList<Point> currPoints = this.getPoints();
		boolean addPoint = true;
		String pointPos = point.getPoint_position();
		String num = pointPos.substring(pointPos.length() -1 , pointPos.length());
		//check for multiplex
		for(int i = 0; i < currPoints.size(); i++)
		{
			String currPos = currPoints.get(i).getPoint_position();
			//if this point is used already
			if(currPos.matches(point.getPoint_position()) || (currPos.matches("[AB]I "+num) && pointPos.matches("[AB]I "+num)))
			{
				//dont add the point
				//if its a multiplex input
				if(currPoints.get(i).getPoint_device().toUpperCase().matches("MULTIPLEX INPUT") && point.getPoint_device().toUpperCase().matches("MULTIPLEX INPUT"))
				{
					//append the description
					currPoints.get(i).appendDescription(point.getPoint_equipment()+" "+point.getPoint_description());
					addPoint = false;
				}
				else
				{
					//show an error
					String errPos = currPos;
					if(currPos.matches("[AB]I [0-9]"))
					{
						errPos = "IN "+num;
					}
					System.out.println("Error: Device "+this.getDeviceNumber()+": Two different devices on point "+errPos);					
				}
			}
		}
		
		if(addPoint == true)
		{
			currPoints.add(point);
			setPoints(currPoints);	
		}	
		
	}
	
	
	public ArrayList<Revision> getRevisions()
	{
		try
		{
			revisions.size();
			return revisions;
		}
		catch(NullPointerException e)
		{
			return new ArrayList<Revision>();
		}
	}
	
	
	public Revision getRevision(int i)
	{
		Revision r = this.getRevisions().get(i);
		return r;
	}
	
	public void setRevisions(ArrayList<Revision> revisions){
		this.revisions = revisions;
	}
	
	public void addRevision(Revision revision)
	{	
		ArrayList<Revision> currRevisions = this.getRevisions();
		currRevisions.add(revision);
		setRevisions(currRevisions);
	}
	
	/**
	 * Check if the point exists based on the point position name
	 * @param point
	 * @return
	 */
	public boolean pointExists(String point)
	{
		for(Point p : points)
		{
			if(p.getPoint_position().matches(point))
			{
				return true;
			}
		}		
		return false;
	}

	/*
	 * Prints out controller info and each point
	 */
	public void printThis(){
		
		System.out.println("");
		System.out.println(
		this.projectName+" "+
		this.controllerName+" "+
		this.deviceNumber+" "+
		this.levelNum+" "+
		this.macAdd+" "+
		this.networkNumber+" "+
		this.additional+" "+
		this.drawNum+" "
		);
		
		ArrayList<Point> points = this.getPoints();
		System.out.println("points");
		for(int i = 0; i < points.size(); i++)
		{
			points.get(i).printThis();			
		}
		
		ArrayList<Revision> revisions = this.getRevisions();
		System.out.println("revisions");
		for(int i = 0; i < revisions.size(); i++)
		{
			revisions.get(i).printThis();
		}
	}


}
