import java.util.ArrayList;


public class Equipment {
	ArrayList<String> controllers;
	ArrayList<String> devices;
	
	public void setControllers(ArrayList<String> controllers)
	{
		this.controllers = controllers;
	}
	
	public void addController(String controller)
	{
		ArrayList<String> c = this.getControllers();
		c.add(controller);
		this.controllers = c;
	}
	
	public ArrayList<String> getControllers()
	{
		try{
			controllers.size();
			return controllers;
		}
		catch(NullPointerException e){
			
			return new ArrayList<String>();
		}
	}
	
	public void setDevices(ArrayList<String> devices)
	{
		this.devices = devices;
	}
	
	public void addDevice(String device)
	{
		ArrayList<String> d = this.getDevices();
		d.add(device);
		this.devices = d;
	}
	
	public ArrayList<String> getDevices()
	{	
		try{
			devices.size();
			return devices;
		}
		catch(NullPointerException e){
			
			return new ArrayList<String>();
		}
	}
}
