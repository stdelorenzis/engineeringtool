package util;

import java.util.ArrayList;
import java.util.Collections;

public class Util {
	/**
	 * Escapes a string
	 * @param str
	 * @return
	 */
	public static String escapeString(String str)
	{
		if(str == null)
		{
			return str;
		}
		return str.replace("'", "''");
	}
	
	
	/**
	 * Adds quotes to each end of the string for db use
	 * @param str
	 * @return
	 */
	public static String escapeAndQuote(String str)
	{
		return "'"+escapeString(str)+"'";
	}
	
	/**
	 * Adds zero for numbers under 10
	 * @param x number to pad
	 * @return new string
	 */
	public static String addZero(int x)
	{
		if(x > 9)
		{
			return ""+x;
		}
		return "0"+x;
	}
	
	public static String removeQuotes(String str)
	{
		if(str == null)
		{
			return str;
		}
		str = str.replace("'", "");
		str = str.replace("\"", "");
		return str;
	}
	
	public static ArrayList<Integer> findMacSwitch(String m, int num_switches)
	{
		ArrayList<Integer> switches = new ArrayList<Integer>();
		if(!m.matches("") && m != null)
		{			
			if(tryParse(m))
			{
				int mac = Integer.parseInt(m);
				ArrayList<Integer> macSwitches = new ArrayList<Integer>(1);
				for(int i = 0; i < num_switches; i++)
				{			
					macSwitches.add((int) Math.pow(2, i));
				}
				Collections.reverse(macSwitches);
				//int[] macSwitches = {1,2,4,8,16,32,64};
				
				for(int i = 0; i < macSwitches.size(); i++)		
				{
					if(mac >= macSwitches.get(i)) 
					{
						switches.add(macSwitches.get(i));
						mac = mac - macSwitches.get(i);
					}
				}
			}
			else
			{
				//not an int
			}
		}
		return switches;
	}
	
	public static float cmToPx(double cm)
	{
		double px = 0;
		px = cm*28.4;		
		return (float)px;
	}
	
	public static boolean tryParse(String value)
	{
		try{
			Integer.parseInt(value);
			return true;
		}
		catch(NumberFormatException e)
		{
			return false;
		}
	}
	
}
